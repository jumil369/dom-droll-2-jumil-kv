// 1) mouseover 

let head = document.getElementsByClassName('logo-heading')
head[0].addEventListener('mouseover', function (event) {
  event.target.style.color = 'orange'

  setTimeout(function () {
    event.target.style.color = ''
  }, 600)
})

// 2) keydown
document.addEventListener('keydown', logKey)

function logKey (e) {
  if (e.code == 'ArrowDown') {
    document.body.style.backgroundColor = '#f3f5c9'
  }else if (e.code == 'ArrowUp') {
    document.body.style.backgroundColor = '#e5cdf7'
  }else if (e.code == 'ArrowLeft') {
    document.body.style.backgroundColor = '#c9eef5'
  }else if (e.code == 'ArrowRight') {
    document.body.style.backgroundColor = '#f7dac3'
  }
}

// 3) Wheel
document.querySelector('.footer > p').addEventListener('wheel', myFunction)

function myFunction () {
  this.style.fontSize = '35px'
}

function zoom(event) {
    event.preventDefault();
  
    scale += event.deltaY * -0.01;
  
    scale = Math.min(Math.max(.125, scale), 4);
  
    el.style.transform = `scale(${scale})`;
  }
  
  let scale = 1;
  const el = document.querySelector('.content-destination > img');
  el.onwheel = zoom;
  el.addEventListener('wheel', zoom);

  // 4) drag / drop
  let p = document.querySelector(".footer");
  p.setAttribute("draggable", true);
  p.setAttribute("ondragstart", "event.dataTransfer.setData('text/plain',null)")
  document.addEventListener("drag", function(event) {
      let drag = document.querySelector(".content-destination");
      drag.setAttribute("ondrop", "drop(event)");
      drag.setAttribute("ondragover", "allowDrop(event)");
}, false);
var dragged;

document.addEventListener("drag", function(event) {

}, false);

document.addEventListener("dragstart", function(event) {
  dragged = event.target;
}, false);

document.addEventListener("dragover", function(event) {
  event.preventDefault();
}, false);


document.addEventListener("drop", function(event) {
  event.preventDefault();
  if (event.target.className == "content-destination") {
    event.target.style.background = "";
    dragged.parentNode.removeChild( dragged );
    event.target.appendChild( dragged );
  }
}, false);

// 5) load
var body = document.getElementsByTagName("body")[0];

document.querySelector("nav a").addEventListener("click",load);

function load(){
    body.addEventListener("load", init())
}
function init() {
    setTimeout(() =>{
        console.log("it works!");
    },1000)
};

// 6) focus

const focus = document.querySelector('nav a');

focus.addEventListener('focus', (event) => {
  event.target.style.color = 'red';    
});

